# EasyWKS Changelog

<!-- BEGIN RELEASE v0.4.2 -->
## Version 0.4.2

Minor feature release

### Changes

<!-- BEGIN CHANGES 0.4.2 -->
- Add option to provide DNS NOTIFY to DANE zone replicas
<!-- END CHANGES 0.4.2-->

<!-- END RELEASE v0.4.2 -->

<!-- BEGIN RELEASE v0.4.1 -->
## Version 0.4.1

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.4.1 -->
- Don't refuse DNS IXFRs and respond with full AXFR
<!-- END CHANGES 0.4.1-->

<!-- END RELEASE v0.4.1 -->

<!-- BEGIN RELEASE v0.4.0 -->
## Version 0.4.0

Feature release

### Changes

<!-- BEGIN CHANGES 0.4.0 -->
- Add authoritative DNS server providing DANE OPENPGPKEY (TYPE61) DNS records
<!-- END CHANGES 0.4.0-->

<!-- END RELEASE v0.4.0 -->

<!-- BEGIN RELEASE v0.3.1 -->
## Version 0.3.1

Feature release

### Changes

<!-- BEGIN CHANGES 0.3.1 -->
- Implement standard and EasyWKS-specific policy flags
- **Deprecation**: The top level option `permit_unsigned_response` is deprecated 
  and will be removed in a future release.  Use the per-domain policy flag
  `me.s3lph.easywks_permit-unsigned-response` instead.
<!-- END CHANGES 0.3.1 -->

<!-- END RELEASE v0.3.1 -->

<!-- BEGIN RELEASE v0.3.0 -->
## Version 0.3.0

Feature release

### Changes

<!-- BEGIN CHANGES 0.3.0 -->
- Set CORS headers on HTTP responses
- Implement direct WKD URLs
- Allow submitting additional revoked keys with the submission request
<!-- END CHANGES 0.3.0 -->

<!-- END RELEASE v0.3.0 -->

<!-- BEGIN RELEASE v0.2.0 -->
## Version 0.2.0

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.2.0 -->
- Release pipeline runs integration test against gpg-wks-client
- Fix minor incompatibilities with gpg-wks-client
- Fix per-domain configuration (e.g. submission-address was not loaded)
<!-- END CHANGES 0.2.0 -->

<!-- END RELEASE v0.2.0 -->

<!-- BEGIN RELEASE v0.1.11 -->
## Version 0.1.11

Feature release

### Changes

<!-- BEGIN CHANGES 0.1.11 -->
- Add easywks import CLI command
<!-- END CHANGES 0.1.11 -->

<!-- END RELEASE v0.1.11 -->

<!-- BEGIN RELEASE v0.1.10 -->
## Version 0.1.10

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.1.10 -->
- RFC 3156 compliance: Don't base64-encode PGP/MIME messages
<!-- END CHANGES 0.1.10 -->

<!-- END RELEASE v0.1.10 -->

<!-- BEGIN RELEASE v0.1.9 -->
## Version 0.1.9

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.1.9 -->
- Proper handling of "Auto-Submitted: no" mail header
- Fix signature verification of responses signed with a subkey
<!-- END CHANGES 0.1.9 -->

<!-- END RELEASE v0.1.9 -->

<!-- BEGIN RELEASE v0.1.8 -->
## Version 0.1.8

### Changes

<!-- BEGIN CHANGES 0.1.8 -->
- Remove LMTP Recipient check as well, leads to trouble with postfix aliasing.
<!-- END CHANGES 0.1.8 -->

<!-- END RELEASE v0.1.8 -->

<!-- BEGIN RELEASE v0.1.7 -->
## Version 0.1.7

### Changes

<!-- BEGIN CHANGES 0.1.7 -->
- Add file locking in order to avoid races between LMTP/process and HTTP.
<!-- END CHANGES 0.1.7 -->

<!-- END RELEASE v0.1.7 -->

<!-- BEGIN RELEASE v0.1.6 -->
## Version 0.1.6

### Changes

<!-- BEGIN CHANGES 0.1.6 -->
- Remove LMTP Envelope-Sender check.
- Debian package now includes an "easywks clean" cronjob.
<!-- END CHANGES 0.1.6 -->

<!-- END RELEASE v0.1.6 -->

<!-- BEGIN RELEASE v0.1.5 -->
## Version 0.1.5

The messages sent by EasyWKS can now be customized. 

### Changes

<!-- BEGIN CHANGES 0.1.5 -->
- Add `responses` config option.
<!-- END CHANGES 0.1.5 -->

<!-- END RELEASE v0.1.5 -->

<!-- BEGIN RELEASE v0.1.4 -->
## Version 0.1.4

Fix HTTP server, compatibility with older HTTP clients

### Changes

<!-- BEGIN CHANGES 0.1.4 -->
- Fix config loading bug in webserver.
- Add `require_user_urlparam` config option that makes the `?l=<user>` query optional. 
<!-- END CHANGES 0.1.4 -->

<!-- END RELEASE v0.1.4 -->

<!-- BEGIN RELEASE v0.1.3 -->
## Version 0.1.3

Compatibility with gpg-wks-client. 

### Changes

<!-- BEGIN CHANGES 0.1.3 -->
- Webserver config is now under a `httpd` key, to be more in line with 
  lmtpd and smtp.
- Add a `permit_unsigned_response` boolean key that, if set to true,
  instructs EasyWKS to accept confirmation responses even if they are
  unsigned, in order to be compatible with version -00 of the draft
  standard, and thus e.g. gpg-wks-client.
- Change the detection logic between submission requests and
  confirmation requests from PGP signature checking to attempting to
  parse the message as a submission response first.
- Add a `__main__` module so that easywks can be invoked as a Python
  module.
<!-- END CHANGES 0.1.3 -->

<!-- END RELEASE v0.1.3 -->

<!-- BEGIN RELEASE v0.1.2 -->
## Version 0.1.2

Fix even morecompatibility issues with aiosmtpd from Debian repo.

### Changes

<!-- BEGIN CHANGES 0.1.2 -->
- Fix #1: lmtpd not working with aiosmtpd 1.2.2
<!-- END CHANGES 0.1.2 -->

<!-- END RELEASE v0.1.2 -->

<!-- BEGIN RELEASE v0.1.1 -->
## Version 0.1.1

Fix compatibility issues with aiosmtpd from Debian repo.

### Changes

<!-- BEGIN CHANGES 0.1.1 -->
- Fix compatibility issues with aiosmtpd from Debian repo.
<!-- END CHANGES 0.1.1 -->

<!-- END RELEASE v0.1.1 -->


<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First release.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- First somewhat stable version.
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
