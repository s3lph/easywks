
from typing import Iterable, List, Tuple

import base64
import hashlib
import secrets
import string
import textwrap
import logging

from twisted.names import dns
from twisted.internet import reactor, defer
from pgpy import PGPKey
from pgpy.constants import SignatureType

from .config import Config


def _zrtp_base32(sha1: bytes) -> str:
    # https://datatracker.ietf.org/doc/html/rfc6189#section-5.1.6
    alphabet = 'ybndrfg8ejkmcpqxot1uwisza345h769'
    encoded = ''
    bits = int.from_bytes(sha1, 'big')
    shift = 155
    for j in range(32):
        n = (bits >> shift) & 31
        encoded += alphabet[n]
        shift -= 5
    return encoded


def hash_user_id(uid: str) -> str:
    if '@' in uid:
        uid, _ = uid.split('@', 1)
    # Only ASCII must be lowercased...
    lower = ''.join(c.lower() if c in string.ascii_uppercase else c for c in uid)
    digest = hashlib.sha1(lower.encode()).digest()
    return _zrtp_base32(digest)


def dane_digest(uid: str) -> str:
    if '@' in uid:
        uid, _ = uid.split('@', 1)
    return hashlib.sha256(uid.encode('utf-8')).hexdigest()[:56]


def create_nonce(n: int = 32) -> str:
    alphabet = string.ascii_letters + string.digits
    nonce = ''.join(secrets.choice(alphabet) for _ in range(n))
    return nonce


def fingerprint(key: PGPKey) -> str:
    return key.fingerprint.upper().replace(' ', '')


def crc24(data: bytes) -> bytes:
    # https://www.rfc-editor.org/rfc/rfc4880#section-6.1
    crc = 0xB704CE
    for b in data:
        crc ^= (b << 16)
        for _ in range(8):
            crc <<= 1
            if crc & 0x1000000:
                crc ^= 0x1864CFB
    return bytes([(crc & 0xff0000) >> 16, (crc & 0xff00) >> 8, (crc & 0xff)])


def armor_keys(keys: List[PGPKey]) -> str:
    joined = b''.join([bytes(k) for k in keys])
    armored = base64.b64encode(joined).decode()
    wrapped = '\n'.join(textwrap.wrap(armored, 64))
    checksum = base64.b64encode(crc24(joined)).decode()
    armored = '-----BEGIN PGP PUBLIC KEY BLOCK-----\n\n' +\
              wrapped + '\n=' + checksum +\
              '\n-----END PGP PUBLIC KEY BLOCK-----\n'
    return armored


def split_revoked(keys: Iterable[PGPKey]) -> Tuple[List[PGPKey], List[PGPKey]]:
    revoked_keys = set()
    for key in keys:
        if len(list(key.revocation_signatures)) == 0:
            continue
        for rsig in key.revocation_signatures:
            if rsig.type == SignatureType.KeyRevocation:
                revoked_keys.add(key)
                break
    key = [k for k in keys if k not in revoked_keys]
    return key, list(revoked_keys)


def dane_notify(domain: str):
    secondaries = Config[domain].dane.get('notify', [])
    if len(secondaries) == 0:
        return
    origin = dns.domainString(f'_openpgpkey.{domain}')
    # this is ugly, but has to do for now
    for host in secondaries:
        try:
            if '@' in host:
                addr, port = host.split('@', 1)
                port = int(port)
            else:
                addr = host
                port = 53
            # Bind a v4 or v6 UDP client socket
            proto = dns.DNSDatagramProtocol(controller=None)
            reactor.listenUDP(0, proto, interface='::' if ':' in addr else '0.0.0.0')
            # Assemble and send NOTIFY message
            m = dns.Message(proto.pickID(), opCode=dns.OP_NOTIFY, auth=1)
            m.queries = [dns.Query(origin, dns.SOA, dns.IN)]
            proto.writeMessage(m, (addr, port))
        except Exception:
            logging.exception(f'An error occurred while attempting to notify {host}')
