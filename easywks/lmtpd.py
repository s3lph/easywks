
import asyncio
import traceback

from aiosmtpd.controller import Controller
from aiosmtpd.lmtp import LMTP

from . import __version__ as version
from .config import Config
from .process import process_mail
from .types import EasyWksError


class LmtpMailServer:

    async def handle_DATA(self, server, session, envelope):
        message = envelope.content
        try:
            process_mail(message)
        except EasyWksError as e:
            return f'550 {e}'
        except BaseException:
            tb = traceback.format_exc()
            return f'550 Error during message processing: {tb}'
        return '250 Message successfully handled'


class LmtpdController(Controller):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def factory(self):
        return LMTP(handler=self.handler, ident=f'EasyWKS {version}', loop=self.loop)


def run_lmtpd(args):
    controller = LmtpdController(handler=LmtpMailServer(), hostname=Config.lmtpd['host'], port=Config.lmtpd['port'])
    controller.start()
    asyncio.get_event_loop().run_forever()
