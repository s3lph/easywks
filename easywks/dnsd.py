from datetime import datetime

from twisted.internet import reactor, defer
from twisted.names import dns, server, common, error
from twisted.python import util as tputil, failure

from .config import Config
from .files import read_dane_public_keys


class Record_OPENPGPKEY(tputil.FancyEqMixin, tputil.FancyStrMixin):
    TYPE = 61
    fancybasename = 'OPENPGPKEY'
    compareAttributes = ('data',)
    showAttributes = ('data',)

    def __init__(self, data=None, ttl=0):
        self.data = data
        self.ttl = ttl

    def encode(self, strio, compDict=None):
        strio.write(self.data)

    def decode(self, strio, length=None):
        self.data = strio.read(length)

    def __hash__(self):
        return hash(self.data)


class DnsServer(common.ResolverBase):

    def __init__(self):
        super().__init__()
        self.zones = {}
        self._load()

    def _load(self):
        for domain in Config.domains:
            origin = dns.domainString(f'_openpgpkey.{domain}')
            self.zones[origin] = domain

    def _make_soa(self, name):
        domain = self.zones[name]
        now = int(datetime.utcnow().timestamp()) // 60
        soa = dns.Record_SOA(mname=Config[domain].dane['soa'].get('mname', 'localhost.'),
                             rname=Config[domain].dane['soa'].get('rname',
                                                                  Config[domain].submission_address.replace('@', '.')),
                             serial=now,
                             refresh=Config[domain].dane['soa'].get('refresh', 300),
                             retry=Config[domain].dane['soa'].get('retry', 60),
                             expire=Config[domain].dane['soa'].get('expire', 2419200),
                             minimum=Config[domain].dane['soa'].get('minimum', 300))
        return dns.RRHeader(name, dns.SOA, payload=soa, auth=True)

    def _make_ns(self, name):
        domain = self.zones[name]
        return [
            dns.RRHeader(name, dns.NS, payload=dns.Record_NS(host), auth=True)
            for host in Config[domain].dane['ns']
        ]

    def _lookup(self, name, cls, type, timeout):
        if name not in self.zones:
            return defer.fail(failure.Failure(dns.AuthoritativeDomainError(name)))
        if type == dns.SOA or type == dns.OPT:
            return defer.succeed(([self._make_soa(name)], [], []))
        if type != dns.AXFR and type != dns.IXFR:
            return defer.fail(failure.Failure(error.DNSQueryRefusedError(name)))
        domain = self.zones[name]
        results = []
        soa = self._make_soa(name)
        ns = self._make_ns(name)
        results.append(soa)
        results.extend(ns)
        for digest, key in read_dane_public_keys(domain).items():
            fqdn = f'{digest}._openpgpkey.{domain}'
            record = Record_OPENPGPKEY(key)
            results.append(dns.RRHeader(dns.domainString(fqdn), record.TYPE, payload=record, auth=True))
        results.append(soa)
        return defer.succeed((results, [], []))


# noinspection PyUnresolvedReferences
# The "reactor" interface is created dynamically, so the listenTCP and run methods only become available during runtime.
def run_dnsd(args):
    auth = DnsServer()
    factory = server.DNSServerFactory(authorities=[auth])
    reactor.listenTCP(Config.dnsd['port'], factory, interface=Config.dnsd['host'])
    reactor.run()
