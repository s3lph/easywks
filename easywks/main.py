
from .config import Config
from .files import init_working_directory, clean_stale_requests
from .process import process_mail_from_stdin, process_key_from_stdin
from .httpd import run_server
from .lmtpd import run_lmtpd
from .dnsd import run_dnsd


import sys
import argparse


def parse_arguments():
    ap = argparse.ArgumentParser(description='OpenPGP WKS for Human Beings')
    ap.add_argument('--config', '-c', metavar='/path/to/config.yml', type=str, nargs=1, default='/etc/easywks.yml')
    sp = ap.add_subparsers(description='EasyWKS understands the following commands:', required=True)

    init = sp.add_parser('init', help='Initialize the EasyWKS working directory and generate the PGP Key. '
                                      'Also called automatically by the other commands.')
    init.set_defaults(fn=None)

    clean = sp.add_parser('clean', help='Clean up stale pending requests. Call this in a cronjob.')
    clean.set_defaults(fn=clean_stale_requests)

    process = sp.add_parser('process', help='Read an incoming mail from stdin and write the response to stdout. '
                                            'Hook this up to your MTA.  Also see lmtpd.')
    process.set_defaults(fn=process_mail_from_stdin)

    server = sp.add_parser('webserver', help='Run a WKD web server. Put this behind a HTTPS-terminating reverse proxy.')
    server.set_defaults(fn=run_server)

    server = sp.add_parser('lmtpd', help='Run a LMTP server to receive mails from your MTA. Also see process.')
    server.set_defaults(fn=run_lmtpd)

    server = sp.add_parser('dnsd', help='Run an authoritative DNS server to provide DANE TYPE61 zones.')
    server.set_defaults(fn=run_dnsd)

    imp = sp.add_parser('import', help='Import a public key from stdin directly into the WKD without WKS verification.')
    imp.add_argument('--uid', '-u', type=str, action='append',
                     help='Limit import to a subset of the key\'s UIDs.  Can be provided multiple times.')
    imp.set_defaults(fn=process_key_from_stdin)

    return ap.parse_args(sys.argv[1:])


def main():
    args = parse_arguments()
    if isinstance(args.config, list):
        conf = args.config[0]
    else:
        conf = args.config
    Config.load_config(conf)
    init_working_directory()
    if args.fn:
        args.fn(args)


if __name__ == '__main__':
    main()
