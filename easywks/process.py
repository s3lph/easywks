import sys
from typing import Any, List, Dict, Tuple

from .crypto import pgp_decrypt
from .mailing import get_mailing_method
from .config import Config, \
    POLICY_MAILBOX_ONLY, EWP_MAX_REVOKED_KEYS, EWP_STRIP_UNVERIFIED_UIDS, EWP_STRIP_3RDPARTY_SIGNATURES, \
    EWP_STRIP_UA_UIDS, EWP_MINIMIZE_REVOKED_KEYS, EWP_PERMIT_UNSIGNED_RESPONSE, POLICY_AUTH_SUBMIT
from .files import read_pending_key, write_public_key, remove_pending_key, write_pending_key
from .types import SubmissionRequest, ConfirmationResponse, PublishResponse, ConfirmationRequest, EasyWksError,\
    XLOOP_HEADER
from .types import fingerprint
from .util import split_revoked

from email.message import MIMEPart, Message
from email.parser import BytesParser
from email.policy import default
from email.utils import getaddresses

from pgpy import PGPMessage, PGPKey, PGPUID, PGPSignature
from pgpy.errors import PGPError


def _get_mime_leafs(msg: Message) -> List[MIMEPart]:
    stack = [msg]
    leafs = []
    while len(stack) > 0:
        node = stack.pop()
        if node.is_multipart():
            stack.extend(node.get_payload())
        else:
            leafs.append(node)
    return leafs


def _get_pgp_message(parts: List[MIMEPart]) -> PGPMessage:
    pgp = None
    for part in parts:
        try:
            p: PGPMessage = PGPMessage.from_blob(part.get_content())
        except ValueError:
            continue
        if p.is_encrypted:
            if pgp is not None:
                raise EasyWksError('More than one encrypted message part')
            pgp = p
    if pgp is None:
        raise EasyWksError('No encrypted message part')
    return pgp


def _get_pgp_publickeys(parts: List[MIMEPart]) -> Tuple[PGPKey, List[PGPKey]]:
    pubkeys: Dict[str, PGPKey] = {}
    for part in parts:
        try:
            _, keys = PGPKey.from_blob(part.get_content())
            for (_, public), key in keys.items():
                if not public:
                    continue
                fpr = fingerprint(key)
                if fpr in pubkeys:
                    raise EasyWksError(f'Key with fingerprint {fpr} appears multiple times in submission request.')
                pubkeys[fpr] = key
        except PGPError:
            pass
    if len(pubkeys) == 0:
        raise EasyWksError('No PGP public key found in the encrypted message part.')
    key, revoked_keys = split_revoked(pubkeys.values())
    if len(key) < 1:
        raise EasyWksError('All of the submitted keys appear to be revoked.')
    elif len(key) > 1:
        fprs = ' '.join([fingerprint(k) for k in key])
        raise EasyWksError(f'More than one non-revoked key was submitted: {fprs}')
    return key[0], revoked_keys


def _parse_submission_request(pgp: PGPMessage, submission: str, sender: str):
    payload = BytesParser(policy=default).parsebytes(pgp.message)
    leafs = _get_mime_leafs(payload)
    valid_key, revoked_keys = _get_pgp_publickeys(leafs)
    sender_uid: PGPUID = valid_key.get_uid(sender)
    if sender_uid is None or sender_uid.email != sender:
        raise EasyWksError(f'Key has no UID that matches {sender}')
    for key in revoked_keys:
        sender_uid: PGPUID = key.get_uid(sender)
        if sender_uid is None or sender_uid.email != sender:
            raise EasyWksError(f'Revoked key {fingerprint(key)} has no UID that matches {sender}')
    return SubmissionRequest(sender, submission, valid_key, revoked_keys)


def _find_confirmation_response(parts: List[MIMEPart]) -> str:
    response = None
    for part in parts:
        if part.get('type', '') == 'confirmation-response':
            # the message wasn't a MIME message; return content as-is
            return str(part)
        c = part.get_content()
        if isinstance(c, bytes):
            try:
                c = c.decode()
            except UnicodeDecodeError:
                # obviously not our part
                continue
        if not isinstance(c, str):
            # not our part either
            continue
        if 'confirmation-response' in c:
            response = c
            continue
    return response


def _parse_confirmation_response(pgp: PGPMessage, submission: str, sender: str):
    payload = BytesParser(policy=default).parsebytes(pgp.message)
    parts = _get_mime_leafs(payload)
    response = _find_confirmation_response(parts)
    rdict: Dict[str, str] = {}
    for line in response.splitlines():
        if ':' not in line:
            continue
        key, value = line.split(':', 1)
        rdict[key.strip()] = value.strip()
    # "from" was renamed to "sender" in draft-koch-openpgp-webkey-service-01.  In addition, gpg-wks-client, which still
    # implements -00, also violates this standard and uses "address" for the sender and "sender" for the submission
    # address.
    if 'from' in rdict:
        rdict['sender'] = rdict['from']
    if 'address' in rdict:
        rdict['sender'] = rdict['address']
    if rdict.get('type', '') != 'confirmation-response':
        raise EasyWksError('Invalid confirmation response: "type" missing or not "confirmation-response"')
    if 'sender' not in rdict or 'nonce' not in rdict:
        raise EasyWksError('Invalid confirmation response: "sender" or "nonce" missing from confirmation response')
    if rdict['sender'] != sender:
        raise EasyWksError(f'Confirmation sender "{rdict["sender"]}" does not match message sender "{sender}"')
    return ConfirmationResponse(rdict['sender'], submission, rdict['nonce'], pgp)


# There is no API for directly removing a PGPUID or PGPSignature object
# noinspection PyProtectedMember
def _apply_submission_policy(request: SubmissionRequest, policy: Dict[str, Any]):
    # Policy: Only permit a certain amount of revoked keys
    maxrevoke = policy.get(EWP_MAX_REVOKED_KEYS, -1)
    if maxrevoke > -1 and len(request.revoked_keys) > maxrevoke:
        raise EasyWksError(f'Submission request contains {len(request.revoked_keys)} revoked keys.  '
                           f'Policy permits not more than {maxrevoke}')
    uid: PGPUID
    revfprs = [fingerprint(request.key)] + [fingerprint(rk) for rk in request.revoked_keys]
    for key in [request.key] + request.revoked_keys:
        # Policy: Strip user attribute (image) UIDs
        if policy.get(POLICY_MAILBOX_ONLY, False) or policy.get(EWP_STRIP_UA_UIDS, False):
            for uid in list(key.userattributes):
                uid._parent = None
                key._uids.remove(uid)
        # Policy: Reject keys as invalid if they contain UIDs with non-empty name or comment parts
        if policy.get(POLICY_MAILBOX_ONLY, False):
            for uid in list(key.userids):
                if uid.email == '' or uid.name != '' or uid.comment != '':
                    raise EasyWksError('This WKS server only accepts UIDs without name and comment parts')
        # Policy: Strip all UIDs except the one being verified
        if policy.get(EWP_STRIP_UNVERIFIED_UIDS, False):
            for uid in list(key.userids):
                if uid.email != request.submitter_address:
                    uid._parent = None
                    key._uids.remove(uid)
        # Policy: Strip all 3rd party signatures from they key
        if policy.get(EWP_STRIP_3RDPARTY_SIGNATURES, False):
            for uid in list(key.userids):
                for sig in list(uid.third_party_certifications):
                    # Keep signatures signed by the revoked keys
                    sig: PGPSignature
                    if sig.signer_fingerprint not in revfprs:
                        uid._signatures.remove(sig)
    # Policy: Produce minimal transportable keys
    if policy.get(EWP_MINIMIZE_REVOKED_KEYS, False):
        for key in request.revoked_keys:
            for uid in list(key.userids):
                # Delete all but the submitter UIDs, and all 3rd party signatures
                if uid.email != request.submitter_address:
                    uid._parent = None
                    key._uids.remove(uid)
                else:
                    for sig in list(uid.third_party_certifications):
                        uid._signatures.remove(sig)
            # Delete UAs
            for uid in list(key.userattributes):
                uid._parent = None
                key._uids.remove(uid)
            # Delete subkeys
            for subkey in key._children.values():
                subkey._parent = None
            key._children.clear()


def process_mail(mail: bytes):
    try:
        msg: Message = BytesParser(policy=default).parsebytes(mail)
        _, sender_mail = getaddresses([msg['from']])[0]
        local, sender_domain = sender_mail.split('@', 1)
    except ValueError:
        raise EasyWksError('Sender mail is not a valid mail address')
    if sender_domain not in Config.domains:
        raise EasyWksError(f'Domain {sender_domain} not supported')
    if msg.get('x-loop', '') == XLOOP_HEADER or msg.get('auto-submitted', 'no') != 'no':
        # Mail has somehow looped back to us, discard
        return
    submission_address: str = Config[sender_domain].submission_address
    try:
        rcpt = getaddresses(msg.get_all('to', []) + msg.get_all('cc', []))
        if len(rcpt) != 1:
            raise EasyWksError('Message has more than one recipients')
        _, rcpt_mail = rcpt[0]
        if rcpt_mail != submission_address:
            raise EasyWksError(f'Message not addressed to submission address {submission_address} '
                               f'for domain {sender_domain}')
        leafs = _get_mime_leafs(msg)
        pgp: PGPMessage = _get_pgp_message(leafs)
        decrypted = pgp_decrypt(sender_domain, pgp)
        payload = BytesParser(policy=default).parsebytes(decrypted.message)
        parts = _get_mime_leafs(payload)
        # First attempt to find a confirmation response.  It's identifiable much easier due to the
        # "type: confirmation-response" part in the message.
        confirmation_response = _find_confirmation_response(parts)
        if confirmation_response is not None:
            request: ConfirmationResponse = _parse_confirmation_response(decrypted, submission_address, sender_mail)
            try:
                key, revoked_keys = read_pending_key(sender_domain, request.nonce)
            except FileNotFoundError:
                raise EasyWksError('There is no submission request for this email address, or it has expired. '
                                   'Please resubmit your submission request.')
            # TODO: Config.permit_unsigned_response is deprecated, but for now retained for backwards compatibility
            if not Config[sender_domain].policy_flags.get(EWP_PERMIT_UNSIGNED_RESPONSE, False) and \
                    not Config.permit_unsigned_response:
                # this throws an error if signature verification fails
                request.verify_signature(key)
            response: PublishResponse = request.get_publish_response(key)
            write_public_key(sender_domain, sender_mail, key, revoked_keys)
            remove_pending_key(sender_domain, request.nonce)
        else:
            request: SubmissionRequest = _parse_submission_request(decrypted, submission_address, sender_mail)
            policy = Config[sender_domain].policy_flags
            _apply_submission_policy(request, policy)
            if policy.get(POLICY_AUTH_SUBMIT, False):
                response = PublishResponse(request.submitter_address, request.submission_address, request.key)
                write_public_key(sender_domain, sender_mail, request.key, request.revoked_keys)
            else:
                response: ConfirmationRequest = request.confirmation_request()
                write_pending_key(sender_domain, response.nonce, request.key, request.revoked_keys)
        rmsg = response.create_signed_message()
    except EasyWksError as e:
        rmsg = e.create_message(sender_mail, submission_address)
    method = get_mailing_method(Config.mailing_method)
    method(rmsg)


def process_mail_from_stdin(args):
    mail = sys.stdin.read().encode()
    process_mail(mail)


def process_key_from_stdin(args):
    try:
        pubkey, _ = PGPKey.from_blob(sys.stdin.read())
    except PGPError:
        raise EasyWksError('Input is not a valid public key.')
    if not pubkey.is_public:
        raise EasyWksError('Input is not a valid public key.')

    for uid in pubkey.userids:
        # Skip user attributes (e.g. photo ids)
        if not uid.is_uid or len(uid.email) == 0:
            continue
        # If a UID filter was provided on the command line, apply it
        if args.uid is not None and len(args.uid) > 0 and uid.email not in args.uid:
            print(f'Skipping ignored email {uid.email}')
            continue
        local, domain = uid.email.split('@', 1)
        # Skip keys we're not responsible for
        if domain not in Config.domains:
            print(f'Skipping foreign email {uid.email}')
            continue
        # All checks passed, importing key
        write_public_key(domain, uid.email, pubkey, [])
        print(f'Imported key {fingerprint(pubkey)} for email {uid.email}')
