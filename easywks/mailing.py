
import smtplib
import email.policy
from email.message import EmailMessage

from .config import Config


def _stdout_mailing(message: EmailMessage):
    print(message.as_string(policy=email.policy.default))


def _smtp_mailing(message: EmailMessage):
    conf = Config.smtp
    cls = smtplib.SMTP
    if conf['tls']:
        cls = smtplib.SMTP_SSL
    with cls(conf['host'], conf['port']) as smtp:
        if not conf['tls'] and conf['starttls']:
            smtp.starttls()
        if conf['username'] is not None:
            smtp.login(conf['username'], conf['password'])
        smtp.send_message(message)
        smtp.quit()


_mailing_methods = {
    'stdout': _stdout_mailing,
    'smtp': _smtp_mailing,
}


def list_mailing_methods():
    return list(_mailing_methods.keys())


def get_mailing_method(method: str):
    return _mailing_methods.get(method)
