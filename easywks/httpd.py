
from .config import Config
from .files import read_hashed_public_key, make_submission_address_file, make_policy_file
from .util import hash_user_id

from bottle import get, run, response, request, HTTPError


CORS = ('Access-Control-Allow-Origin', '*')


def abort(code, text):
    err = HTTPError(code, text)
    err.add_header(*CORS)
    return err


def get_domain_header():
    domain = request.get_header('host')
    if len(domain) != 1 or domain[0] is None:
        abort(400, 'Bad Request')
    return domain[0]


@get('/.well-known/openpgpkey/<domain>/submission-address')
def advanced_submission_address(domain: str):
    if domain not in Config.domains:
        abort(404, 'Not Found')
    response.add_header('Content-Type', 'text/plain')
    response.add_header(*CORS)
    return make_submission_address_file(domain)


@get('/.well-known/openpgpkey/submission-address')
def direct_submission_address():
    return advanced_submission_address(get_domain_header())


@get('/.well-known/openpgpkey/<domain>/policy')
def advanced_policy(domain: str):
    if domain not in Config.domains:
        abort(404, 'Not Found')
    response.add_header('Content-Type', 'text/plain')
    response.add_header(*CORS)
    return make_policy_file(domain)


@get('/.well-known/openpgpkey/policy')
def direct_policy():
    return advanced_policy(get_domain_header())


@get('/.well-known/openpgpkey/<domain>/hu/<userhash>')
def advanced_hu(domain: str, userhash: str):
    if domain not in Config.domains:
        abort(404, 'Not Found')
    if Config.httpd['require_user_urlparam']:
        userid = request.query.l
        if not userid or hash_user_id(userid) != userhash:
            abort(404, 'Not Found')
    try:
        pubkey, revoked = read_hashed_public_key(domain, userhash)
        response.add_header('Content-Type', 'application/octet-stream')
        response.add_header(*CORS)
        return bytes(pubkey[0]) + b''.join([bytes(k) for k in revoked])
    except FileNotFoundError:
        abort(404, 'Not Found')


@get('/.well-known/openpgpkey/hu/<userhash>')
def direct_hu(userhash: str):
    return advanced_hu(get_domain_header(), userhash)


def run_server(args):
    run(host=Config.httpd['host'], port=Config.httpd['port'])


if __name__ == '__main__':
    run_server(None)
