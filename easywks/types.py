
from typing import List

from datetime import datetime
from email.encoders import encode_noop
from email.policy import default
from email.utils import format_datetime
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText

from pgpy import PGPKey, PGPMessage, PGPUID
from pgpy.errors import PGPError

from .crypto import pgp_sign
from .config import render_message
from .util import create_nonce, fingerprint


XLOOP_HEADER = 'EasyWKS'


class SubmissionRequest:

    def __init__(self, submitter_addr: str, submission_addr: str, key: PGPKey, revoked_keys: List[PGPKey]):
        self._submitter_addr = submitter_addr
        self._submission_addr = submission_addr
        self._key = key
        self._revoked_keys = revoked_keys

    def confirmation_request(self) -> 'ConfirmationRequest':
        return ConfirmationRequest(self._submitter_addr, self. _submission_addr, self._key)

    @property
    def submitter_address(self):
        return self._submitter_addr

    @property
    def submission_address(self):
        return self._submission_addr

    @property
    def key(self):
        return self._key

    @property
    def revoked_keys(self):
        return list(self._revoked_keys)


class ConfirmationRequest:

    def __init__(self, submitter_addr: str, submission_addr: str, key: PGPKey, nonce: str = None):
        self._domain = submitter_addr.split('@')[1]
        self._submitter_addr = submitter_addr
        self._submission_addr = submission_addr
        self._key = key
        self._nonce = nonce or create_nonce()

    @property
    def domain(self):
        return self._domain

    @property
    def submitter_address(self):
        return self._submitter_addr

    @property
    def submission_address(self):
        return self._submission_addr

    @property
    def key(self):
        return self._key

    @property
    def nonce(self):
        return self._nonce

    def create_signed_message(self):
        mail_text = render_message('confirm',
                                   domain=self.domain,
                                   sender=self.submitter_address,
                                   submission=self.submission_address)
        mpplain = MIMEText(mail_text, _subtype='plain')
        ps = '\r\n'.join([
            'type: confirmation-request',
            f'sender: {self._submission_addr}',
            f'address: {self._submitter_addr}',
            f'fingerprint: {fingerprint(self._key)}',
            f'nonce: {self._nonce}',
            ''
        ])
        payload = MIMEText(ps, _subtype='plain')
        to_encrypt = PGPMessage.new(payload.as_string(policy=default))
        encrypted = self._key.encrypt(to_encrypt)
        mpenc = MIMEApplication(str(encrypted), _subtype='vnd.gnupg.wks')
        mixed = MIMEMultipart(_subtype='mixed', _subparts=[mpplain, mpenc])
        to_sign = PGPMessage.new(mixed.as_string(policy=default).replace('\n', '\r\n'))
        sig = pgp_sign(self.domain, to_sign)
        mpsig = MIMEApplication(str(sig), _subtype='pgp-signature', name='signature.asc', _encoder=encode_noop)
        mpsig['Content-Description'] = 'OpenPGP digital signature'
        mpsig['Content-Disposition'] = 'attachment; filename="signature"'
        email = MIMEMultipart(_subtype=f'signed', _subparts=[mixed, mpsig], policy=default,
                              protocol='application/pgp-signature')
        email.set_param('micalg', f'pgp-{str(sig.hash_algorithm).split(".",1)[1].lower()}', requote=False)
        email['Subject'] = 'Confirm your key publication'
        email['To'] = self._submitter_addr
        email['From'] = self._submission_addr
        email['Date'] = format_datetime(datetime.utcnow())
        email['Wks-Draft-Version'] = '3'
        email['Wks-Phase'] = 'confirm'
        email['X-Loop'] = XLOOP_HEADER
        email['Auto-Submitted'] = 'auto-replied'
        return email


class ConfirmationResponse:

    def __init__(self, submitter_addr: str, submission_addr: str, nonce: str, msg: PGPMessage):
        self._domain = submitter_addr.split('@')[1]
        self._submitter_addr = submitter_addr
        self._submission_addr = submission_addr
        self._nonce = nonce
        self._msg = msg

    @property
    def domain(self):
        return self._domain

    @property
    def submitter_address(self):
        return self._submitter_addr

    @property
    def submission_address(self):
        return self._submission_addr

    @property
    def nonce(self):
        return self._nonce

    def get_publish_response(self, key: PGPKey) -> 'PublishResponse':
        return PublishResponse(self._submitter_addr, self._submission_addr, key)

    def verify_signature(self, key: PGPKey):
        if not self._msg.is_signed:
            raise EasyWksError('The confirmation response is not signed. If you used an automated tool such as '
                               'gpg-wks-client for submitting your response, please update said tool or try '
                               'responding manually.')
        uid: PGPUID = key.get_uid(self._submitter_addr)
        if uid is None or uid.email != self._submitter_addr:
            raise EasyWksError(f'UID {self._submitter_addr} not found in PGP key')
        try:
            # Should raise an error when verification fails, but add the boolean check as a additional protection
            if not key.verify(self._msg):
                raise EasyWksError(f'PGP signature could not be verified')
        except PGPError as e:
            raise EasyWksError(f'PGP signature could not be verified: {e}')


class PublishResponse:

    def __init__(self, submitter_addr: str, submission_addr: str, key: PGPKey):
        self._domain = submitter_addr.split('@')[1]
        self._submitter_addr = submitter_addr
        self._submission_addr = submission_addr
        self._key = key

    @property
    def submitter_address(self):
        return self._submitter_addr

    @property
    def submission_address(self):
        return self._submission_addr

    @property
    def key(self):
        return self._key

    @property
    def domain(self):
        return self._domain

    def create_signed_message(self):
        mail_text = render_message('done',
                                   domain=self.domain,
                                   sender=self.submitter_address,
                                   submission=self.submission_address)
        mpplain = MIMEText(mail_text, _subtype='plain')
        to_encrypt = PGPMessage.new(mpplain.as_string(policy=default))
        to_encrypt |= pgp_sign(self.domain, to_encrypt)
        encrypted: PGPMessage = self.key.encrypt(to_encrypt)
        payload = MIMEApplication(str(encrypted), _subtype='octet-stream', _encoder=encode_noop)
        mpenc = MIMEApplication('Version: 1\r\n', _subtype='pgp-encrypted', _encoder=encode_noop)
        email = MIMEMultipart(_subtype='encrypted', _subparts=[mpenc, payload], policy=default,
                              protocol='application/pgp-encrypted')
        email['Subject'] = 'Your key has been published'
        email['To'] = self.submitter_address
        email['From'] = self.submission_address
        email['Date'] = format_datetime(datetime.utcnow())
        email['Wks-Draft-Version'] = '3'
        email['Wks-Phase'] = 'done'
        email['X-Loop'] = XLOOP_HEADER
        email['Auto-Submitted'] = 'auto-replied'
        return email


class EasyWksError(BaseException):

    def __init__(self, msg: str, ):
        super().__init__()
        self._msg = msg

    def __str__(self) -> str:
        return self._msg

    def create_message(self, submitter_addr: str, submission_addr: str) -> MIMEText:
        domain = submission_addr.split('@', 1)[1]
        mail_text = render_message('error',
                                   domain=domain,
                                   sender=submitter_addr,
                                   submission=submission_addr,
                                   error=self._msg)
        email = MIMEText(mail_text)
        email['Subject'] = 'An error has occurred while processing your request'
        email['From'] = submission_addr
        email['To'] = submitter_addr
        email['Date'] = format_datetime(datetime.utcnow())
        email['Wks-Draft-Version'] = '3'
        email['Wks-Phase'] = 'error'
        email['X-Loop'] = XLOOP_HEADER
        email['Auto-Submitted'] = 'auto-replied'
        return email
