from setuptools import setup, find_packages

from easywks import __version__


setup(
    name='easywks',
    version=__version__,
    author='s3lph',
    author_email='account-gitlab-ideynizv@kernelpanic.lol',
    description='OpenPGP WKS for Human Beings',
    license='MIT',
    keywords='pgp,wks',
    url='https://gitlab.com/s3lph/easywks',
    packages=find_packages(exclude=['*.test']),
    install_requires=[
        'aiosmtpd',
        'bottle',
        'dnspython',
        'PyYAML',
        'PGPy',
        'Twisted',
    ],
    entry_points={
        'console_scripts': [
            'easywks = easywks.main:main'
        ]
    },
)
