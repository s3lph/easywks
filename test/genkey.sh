#!/bin/bash

cat >/tmp/keygen <<EOF
%no-protection
%no-ask-passphrase
%transient-key
Key-Type: EDDSA
Key-Curve: ed25519
Subkey-Type: ECDH
Subkey-Curve: cv25519
Expire-Date: 0
Name-Real: EasyWKS Test User
Name-Comment: TEST KEY DO NOT USE
Name-Email: ${1}
EOF

gpg --batch --full-gen-key /tmp/keygen
for uid in $@; do
    gpg --batch --quick-add-uid "${1}" "EasyWKS Test User (TEST KEY DO NOT USE) <${uid}>"
done
gpg --export --armor "${1}" > "/tmp/${1}.asc"
for uid in $@; do
    gpg --export --armor "${uid}" > "/tmp/${uid}.asc"
done
